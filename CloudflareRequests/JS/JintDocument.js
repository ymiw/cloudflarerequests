﻿/**
 * This is meant to provide basic document functionality to Jint for Cloudflare's scripts to use.
 * 
 * This should be loaded into the JSEngine before the Cloudflare script's code is executed.
 */

/*
 * Globals required from C# code:
 * 
 *  PAGE_URL - url of the page*/

// Get globals, bail if they're not set.
PAGE_URL = PAGE_URL || undefined;

if (typeof PAGE_URL === "undefined") {
    log("MISSING REQUIRED GLOBAL VARIABLE PAGE_URL!");
}

if (typeof document === "undefined") {
    // log("Document isn't defined, providing basic functionality...");

    // Providing basic document functionality seems easier than modifying the code, and 
    // if the code changes a bit the functionality should hopefully cover the change...
    document = {
        elements: new Array(),

        createElement: function(name) {
            this.elements.push({
                name: name,
                id: null,
                value: null,
                innerHTML: null,
                action: null,
                firstChild: {
                    href: PAGE_URL /* This may be ghett0 but it will suffice. */
                },
                submit: function () { }, // Filler function
                setValue: function(valName, val) { // Quick utility function
                    this[valName] = val;
                    return this;
                }
            });

            return this.elements[this.elements.length - 1];
        },

        getElementById: function (id) {
            for (var i = 0; i < this.elements.length; i++) {
                if (this.elements[i] !== null && this.elements[i].id === id) {
                    return this.elements[i];
                }
            }

            return null;
        }
    };
}
