﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudflareRequests
{
    public class CFVariableParsingFailedException : Exception
    {
        public CFVariableParsingFailedException(string message) : base(message)
        {
        }
    }
}
