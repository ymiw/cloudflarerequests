﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Jint;
using Jint.Runtime;

namespace CloudflareRequests
{
    public static class CFRequestGenerator
    {
        /// <summary>
        /// List of cookies for various Cloudflare hosts that have been added.
        /// </summary>
        private static List<CFCookies> _Cookies = new List<CFCookies>();

        /// <summary>
        /// List of hosts that have been added, but aren't Cloudflare hosts.
        /// </summary>
        private static List<string> _NonCFHosts = new List<string>();

        // JintDocument.js code 
        // TODO: Maybe loading should be removed, as it's unlikely that JintDocument.js will need to be modified...
        private static string _JintDocumentCode;

        //
        // Regexes for Cloudflare response
        //
        private static Regex
            _reChallengeForm = new Regex("id=\"challenge-form\" action=\"(.*?)\"", RegexOptions.IgnoreCase | RegexOptions.Compiled),
            _reJSCHL = new Regex("type=\"hidden\" name=\"jschl_vc\" value=\"(.*?)\"", RegexOptions.IgnoreCase | RegexOptions.Compiled),
            _rePass = new Regex("type=\"hidden\" name=\"pass\" value=\"(.*?)\"", RegexOptions.IgnoreCase | RegexOptions.Compiled),
            _reJSCode = new Regex(@"\/\/\<\!\[CDATA\[[\s\S]+?setTimeout\(function\(\)\{([\s\S]+?.\.submit\(\));", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        /// <summary>
        /// UserAgent to use while sending requests.
        /// </summary>
        public static string UserAgent = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36";
        
        /// <summary>
        /// Attempts to initialize Cloudflare's cookies on the specified uri's host.
        /// </summary>
        /// <param name="uri">A uri to a page on the host that uses Cloudflare.</param>
        /// <returns>True if the Cloudflare cookies were succesfully obtained, false otherwise.</returns>
        public static async Task<bool> AddHostAsync(Uri uri)
        {
            // TODO: I don't believe this will handle sub-domains properly
            // (eg: xdcc.horriblesubs.info) so we need to fix that.
            var safeHostUri = uri.Scheme + "://" + uri.Host + "/";

            if (_NonCFHosts.Contains(safeHostUri.ToLower()))
                return true;

            if (_JintDocumentCode == null)
            {
                var reader = new StreamReader(File.OpenRead("JS/JintDocument.js"));
                _JintDocumentCode = await reader.ReadToEndAsync();
                reader.Close();

                if (string.IsNullOrEmpty(_JintDocumentCode))
                    throw new Exception("Couldn't load JintDocument.js! Is the file empty or missing?");
            }

            var cookieContainer = new CookieContainer();
            HttpWebResponse initialResponse;

            try
            {
                var initialRequest = WebRequest.CreateHttp(uri);
                initialRequest.UserAgent = UserAgent;
                initialRequest.AllowAutoRedirect = false;
                initialRequest.CookieContainer = cookieContainer;

                // This will raise a WebException since Cloudflare returns a 503 response.
                initialResponse = await initialRequest.GetResponseAsync() as HttpWebResponse;

                // ... and if the exception isn't thrown, it's probable this hosn't is using cloudflare
                Debug.WriteLine("Found non cloudflare host " + uri.Host.ToLower());
                _NonCFHosts.Add(uri.Host.ToLower());
                return true;
            }
            catch (WebException webException)
            {
                var response = webException.Response as HttpWebResponse;

                if (response == null)
                    return false;

                if (response.StatusCode == HttpStatusCode.ServiceUnavailable)
                    initialResponse = response;
                else
                    // The only exception we should handle is Cloudflare's 503; anything else
                    // may be an exception that should be handled elsewhere.
                    throw;
            }

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (initialResponse == null)
                // ReSharper disable once HeuristicUnreachableCode
                return false;

            // We're expecting to get __cfduid cookie from the initial request
            // If it doesn't exist, the site likely isn't enforcing cloudflare.
            // var hasCfduid = false;
            Cookie cfduidCookie = null;

            foreach (Cookie cookie in initialResponse.Cookies)
            {
                if (cookie.Name.Equals("__cfduid", StringComparison.OrdinalIgnoreCase))
                {
                    // hasCfduid = true;
                    cfduidCookie = cookie;
                    break;
                }
            }

            // if (!hasCfduid)
            if (cfduidCookie == null)
            {
                Debug.WriteLine("Found non cloudflare host " + uri.Host.ToLower());
                _NonCFHosts.Add(uri.Host.ToLower());
                return false;
            }
            
            var responseReader = new StreamReader(initialResponse.GetResponseStream());
            var body = await responseReader.ReadToEndAsync();
            var formAction = _reChallengeForm.Match(body);
            var jschl = _reJSCHL.Match(body);
            var pass = _rePass.Match(body);
            var jsCode = _reJSCode.Match(body);

            if (!formAction.Success || !jschl.Success || !pass.Success || !jsCode.Success)
            {
                throw new CFVariableParsingFailedException($"Failed parsing cloudflare variables on page {uri.AbsoluteUri}\nMatches:" + 
                    $"formAction:{formAction.Success}, jschl: {jschl.Success}, pass: {pass.Success}, jsCode: {jsCode}");
            }

            // Now we need to execute the Cloudflare code using the JS engine
            //
            // However, document doesn't exist when using Jint so we need to mimic its functionality by 
            // using JintDocument.js' source
            var engine = new Engine()
                .SetValue("log", new Action<object>(Console.WriteLine))
                .SetValue("PAGE_URL", safeHostUri); // REQUIRED by JintDocument.js
            string jschlAnswer = "";

            try
            {
                // Provide document functionality for Cloudflare
                engine.Execute(_JintDocumentCode);

                // Provide the needed cloudflare elements 
                engine.Execute($@"
                    document.createElement(null)
                        .setValue('id', 'challenge-form')
                        .setValue('action', '{formAction.Groups[1].Value}');
                        // .setValue('type', 'form'); // Not really needed, though.
                    document.createElement('jschl_answer')
                        .setValue('id', 'jschl-answer');
                        //.setValue('type', 'hidden');
                ");

                // Execute cloudflare code then get the jschl-answer value
                engine.Execute(jsCode.Groups[1].Value);

                var jschlAnswerObj = engine.Execute("document.getElementById('jschl-answer').value").GetCompletionValue();
                jschlAnswer = ((int)jschlAnswerObj.AsNumber()).ToString();
                Debug.WriteLine($"jschlAnswer: {jschlAnswer}");
            }
            catch (JavaScriptException jse)
            {
                Console.Error.WriteLine("JAVASCRIPT ERROR IN CLOUDFLAREREQUESTS (uh oh!):");
                Console.Error.WriteLine("Source: " + jse.Location.Source);
                Console.Error.WriteLine("Error: " + jse.Error);
                Console.Error.WriteLine("Column: " + jse.Column);
                Console.Error.WriteLine("Line: " + jse.LineNumber);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Couldn't load JS code!");
                Console.Error.WriteLine(ex);
            }

            // Wait 5 seconds (to be safe, sometimes cloudflare doesn't play nice for whatever reason)
            // and proceed
            await Task.Delay(5000);

            // Execute final request to cloudflare page and store the cookies
            var finalRequest = WebRequest.CreateHttp($"{safeHostUri}{formAction.Groups[1].Value}?jschl_vc={jschl.Groups[1].Value}&pass={pass.Groups[1].Value}&jschl_answer={jschlAnswer}");
            finalRequest.AllowAutoRedirect = false;
            finalRequest.UserAgent = UserAgent;
            finalRequest.CookieContainer = cookieContainer;

            var finalResponse = await finalRequest.GetResponseAsync() as HttpWebResponse;
            // var hasClearance = false;
            Cookie clearanceCookie = null;

            foreach (Cookie cookie in finalResponse.Cookies)
            {
                if (cookie.Name.Equals("cf_clearance", StringComparison.OrdinalIgnoreCase))
                {
                    // hasClearance = true;
                    clearanceCookie = cookie;
                    break;
                }
            }

            // if (!hasClearance)
            if (clearanceCookie == null)
                return false;

            // TODO: Store this host properly
            var newCookies = new CFCookies(new Uri(safeHostUri), cfduidCookie, clearanceCookie);
            _Cookies.Add(newCookies);
            return true;
        }

        /// <summary>
        /// Generates a request to the target url.
        /// </summary>
        /// <param name="uri">The URL to access.</param>
        /// <returns>A request with Cloudflare cookies set or null if the Generator hasn't initialized this site.</returns>
        public static HttpWebRequest CreateHttp(Uri uri)
        {
            if (_Cookies.Count == 0 && _NonCFHosts.Count == 0)
                throw new Exception("CFRequestGenerator must have hosts added to it before being used!");

            // var hasCookie = false;
            CFCookies cfCookies = null;

            foreach (var cookie in _Cookies)
            {
                if (cookie.HostUri.Host.Equals(uri.Host, StringComparison.OrdinalIgnoreCase))
                {
                    // hasCookie = true;
                    cfCookies = cookie;
                    break;
                }
            }

            // if (!hasCookie)
            if (cfCookies == null)
                return null;
            
            var request = WebRequest.CreateHttp(uri);
            request.UserAgent = UserAgent;

            // Add stored cloudflare cookies
            request.CookieContainer = new CookieContainer();
            request.CookieContainer.Add(cfCookies.CfduidCookie);
            request.CookieContainer.Add(cfCookies.ClearanceCookie);

            return request;
        }

        //
        // Ease of access functions
        //

        public static async Task<bool> AddHostAsync(string uri)
        {
            return await AddHostAsync(new Uri(uri));
        }

        public static bool AddHost(Uri uri)
        {
            var result = AddHostAsync(uri);
            result.Wait();

            return result.Result;
        }

        public static bool AddHost(string uri)
        {
            return AddHost(new Uri(uri));
        }

        public static HttpWebRequest CreateHttp(string uri)
        {
            return CreateHttp(new Uri(uri));
        }
    }
}
