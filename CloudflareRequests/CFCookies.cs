﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CloudflareRequests
{
    /// <summary>
    /// Stores Cloudflare cookie data.
    /// </summary>
    public class CFCookies
    {
        public Uri HostUri { get; set; }
        public Cookie CfduidCookie { get; set; }
        public Cookie ClearanceCookie { get; set; }

        public CFCookies(Uri host, Cookie cfduid, Cookie clearance)
        {
            HostUri = host;
            CfduidCookie = cfduid;
            ClearanceCookie = clearance;
        }
    }
}
