This library was built to provide basic functionality to make web requests to Cloudflare protected websites. 

# NOTES

It requires Jint for the Javascript engine (as it attempts to replicate Cloudflare's behaviour similar to how a browser loads the page).

# LICENSE

This may change later, but for now please see LICENSE.md.